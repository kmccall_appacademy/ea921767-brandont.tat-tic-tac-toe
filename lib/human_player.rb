class HumanPlayer
  attr_reader :name
  attr_accessor :mark

  def initialize(name)
    @name = name
  end

  def get_move
    puts "Where would you like to move? (row, col)"
    gets.chomp.split(',').map(&:to_i)
  end

  def display(board)
    puts board.grid
  end
end
