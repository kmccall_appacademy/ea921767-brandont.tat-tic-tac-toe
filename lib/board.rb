class Board
  attr_reader :grid, :marks

  def initialize(grid = nil)
    @grid = grid || Array.new(3) {Array.new(3)}
    @marks = [:O, :X]
  end

  def place_mark(pos, mark)
    @grid[pos.first][pos.last] = mark
  end

  def empty?(pos)
    @grid[pos.first][pos.last] == nil
  end

  def winner
    @marks.each do |mark|
      return mark if win?(mark)
    end

    nil
  end

  def over?
    return true if winner || @grid.flatten.none?{|pos| pos.nil?}
  end

  private

  def win?(mark)
    lines = (row_winner + col_winner + diag_winner)

    lines.any? do |line|
      line == [mark, mark, mark]
    end
  end

  def row_winner
    rows = []

    (0..2).map do |row|
      rows << [@grid[row][0], @grid[row][1], @grid[row][2]]
    end

    rows
  end

  def col_winner
    cols = []

    (0..2).map do |col|
      cols << [@grid[0][col], @grid[1][col], @grid[2][col]]
    end

    cols
  end

  def diag_winner
    left_diag = [@grid[2][0], @grid[1][1], @grid[0][2]]
    right_diag = [@grid[0][0], @grid[1][1], @grid[2][2]]

    [left_diag, right_diag]
  end
end
