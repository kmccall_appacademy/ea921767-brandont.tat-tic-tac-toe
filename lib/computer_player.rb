class ComputerPlayer
  attr_reader :name, :board
  attr_accessor :mark

  def initialize(name)
    @name = name
  end

  def display(board)
    @board = board
  end

  def get_move
    valid_moves = []
    (0..2).each do |row|
      (0..2).each do |col|
        valid_moves << [row, col] if board.empty?([row, col])
      end
    end

    valid_moves.each_with_index do |move, i|
      return valid_moves[i] if winning_move?(move)
    end

    valid_moves[rand(0...valid_moves.size)]
  end

  private

  def winning_move?(pos)
    board.place_mark(pos, mark)

    if board.winner == mark
      board.place_mark(pos, nil)
      true
    else
      board.place_mark(pos, nil)
      false
    end
  end
end
